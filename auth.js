const jwt = require("jsonwebtoken");
//user defined string data that will be used to create our JSON web tokens
const secret = "ECommerceAPI";

module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};

	//generate a JSON web token using the jwt's sign method
	//generate token using the data, and the secret with no additional options provided
	return jwt.sign(data, secret, {});
};

module.exports.verify = (req, res, next)=>{
	//this token is retrieved from the request header -> authorization
		//postman - Authorization > Bearer Token
	let token = req.headers.authorization;
	if(typeof token !== "undefined"){
		console.log(token);
		//the slice method takes only the token from the info sent via the request header
		//the token sent is a type of "bearer" token which when received contains the word "bearer" as a prefix to the string
		token = token.slice(7, token.length);
		//validate the token using the "verify" method decrypting the token using the secret code
		return jwt.verify(token, secret, (err,data)=>{
			//if the JWT is not valid
			if(err){
				res.send({auth: "failed"});
			//JWT is valid
			}else{
				//allows the app to proceed with the next middleware function in the route
				next();
			};
		});
	//token does not exist
	}else{
		return res.send({auth: "failed"});
	};
};

//Token decryption
/*
	- Analogy
		Open the gift and get the content
*/
module.exports.decode = (token)=>{
	// Token recieved and is not undefined
	if(typeof token !== "undefined"){
		// Retrieves only the token and removes the "Bearer " prefix
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (err, data)=>{
			if(err){
				return null;
			}else{
				// The "decode" method is used to obtain the information from the JWT
				// The "{complete:true}" option allows us to return additional information from the JWT token
				// Returns an object with access to the "payload" property which contains user information stored when the token was generated
				// The payload contains information provided in the "createAccessToken" method defined above (e.g. id, email and isAdmin)
				return jwt.decode(token,{complete:true}).payload;
			};
		});
	// Token does not exist
	}else{
		return null;
	};
};