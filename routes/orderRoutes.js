const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderControllers");
const productController = require("../controllers/productControllers");
const auth = require("../auth");
const Order = require("../models/Order");
const Product = require("../models/Product");


//create Order
router.post("/createOrder", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  orderController.checkOut(userData.id, req.body).then((resultFromController) => res.send(resultFromController));
});


//Get all orders

router.get("/all",auth.verify,(req,res)=>{
  orderController.getAllOrders(req.params).then(resultFromController=>res.send(resultFromController));
});

module.exports = router;