const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
const orderRoutes = require("./routes/orderRoutes");

const app = express();


app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

mongoose.set('strictQuery',true);

mongoose.connect("mongodb+srv://admin:admin123@clusterbatch248.bpiajdg.mongodb.net/test?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection;
db.on("error", console.error.bind(console,"connection error"));
db.once("open", ()=>console.log("Hi! We are connected to MongoDB Atlas!"));

app.use("/users", userRoutes);

app.use("/products", productRoutes);

app.use("/orders", orderRoutes);


const PORT = process.env.PORT || 4000;

app.listen(PORT, ()=> {
  console.log(`API is now online on port ${PORT}`);
});




